# Command Groups

After fiddling around with commands with arguments, checks and cooldowns, you probably came across an organisation problem. Imagine you have a greeter system that welcomes new people and says goodbye when they leave, and you make it customisable and toggleable. Well, you would probably have a couple of commands to make that customisation happen, maybe something like `togglegreeter` and `customisegreeter`. It's not that big of an issue for just these two commands, but they have already quite large names. Now, if you add more options later on, you would start to have a problem sorting those commands as they become harder to remember and less "pretty" from a interface design perspective. This is where command groups fit in perfectly. Command groups are an easy way to sort and nest commands in such a way that they share some attributes.  

The way you create command groups is really similar to how you create commands. You use the `bot.group` decorator, which takes basically all the parameters the command decorator does, while also having some new ones.  
Here's an example:
```python
@bot.group(name="foo")
async def foo_group(ctx):
    ...
```
Now, the logic you put in the command group function will be executed every time the group is called (with or without a subcommand), unless you pass the `invoke_without_command=True` parameter in its decorator, in which case it will only get run when no subcommand is called. Case insensitivity is group dependent, meaning that if you pass `case_insentivie=True` on the bot constructor, it will not affect groups. For that you have to pass that parameter in the group decorator.

Creating a subcommand is also really easy and very, very similar to a normal root command. This time you just use `whatever_the_parent_group_function_is_called.command` instead of `bot.command`. Let me show you:
```python
@bot.group(name="foo", invoke_without_command=True)
async def foo_group(ctx):
    await ctx.send("No subcommand was found!")

@foo_group.command(name="bar")
async def bar_subcommand(ctx):
    await ctx.send("You invoked the `bar` subcommand!")
```
Typing `!foo` (assuming the prefix is `!`) would make the bot reply with "No subcommand was found!" and `!foo bar` would result in the "You invoked the `bar` subcommand" answer. 

You can nest commands and even groups inside other groups as much as you like. However, for some unknown reason, checks and cooldowns don't get passed on if you set the `invoke_without_command` parameter to `True` in the group decorator. *[**`libneko`**](./need-to-knows.html#libneko) fixes that in its command framework mod.*

---

That's all for this section! I hope you understood how to organise and nest your commands in order to provide a better interface in your bot. The next section is [**Error Handling**](./error-handling.html) where I explain a really fundamental part of any good bot.
