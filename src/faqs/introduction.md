# FAQs
FAQs/guide tutorials.
## General/Resources
- [APIs](apis.md)
- [Enviroment Variables](env-vars.md)
- [Glitch Hosting](glitch-hosting.md)
- [Leaked Token Recovery](token-leaked.md)
## Python
- [Installing Python](installing-python.md)
- [Decorators](python-decorators)
- [Venvs](python-venvs)
